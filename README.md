# Cars

Cars is a sample project written in C++ to demonstrate some OOP concepts. All it does is behaving like you had a real car and could complete actions like buying a new car, tanking, driving etc. In reality nothing really happens.

## Running a program

Go ahead and compile it with:

```bash
g++ src/main.cpp -o car.o
```
then just run it simply as:

```bash
./car.o
```

## Sample usage

```
Hello :)
Here's the list of available commands:
h - prints help
n - opens a new car wizard
l - lists current cars
c - chooses a car from the list
m - lists actions for a car
d - deletes a car
-> n
What brand car is going to be? (Mazda, Fiat, Opel): Fiat
Please enter a proper car model: 125p
Please enter car's colouring: Red
What's the capacity for the fuel? 15
Car has been added succesfully
-> l
1. Red Fiat 125p
-> c
Choose an active car from the list:
1. Red Fiat 125p
your decision: 1
1. Red Fiat 125p has been set to active car
-> m
Here is the list for actions taken to an active car:
[open|close] [left|right|boot] - opens/closes specified doors
get in - gets driver into a car
tank - tanks fuel to the car
[start|stop] engine - starts/stops engine
drive [forward|backwards] - drives forwards or backwards
turn [left|right] - turns specified direction. Note that the car must be turned on.
turn [on|off] lights - turns on/off daily lights.

-> drive forward
an exception has occurred: Cannot drive! engine is not ready yet! Check if it is running
-> tank
How many litres of fuel would you like to tank?: 10
Tanked 10 litres successfully
-> start engine
Engine has been started
-> drive forward
an exception has occurred: Please turn on your daily lights before starting a drive
-> turn on lights
daily lights are now turned on!
-> drive forward
Driving forward
-> turn left
Using left winker...
Driving left

