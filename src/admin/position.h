#ifndef PROJEKT_POSITION_H
#define PROJEKT_POSITION_H

enum Position {
    LEFT,
    RIGHT,
    BACKWARDS,
    FORWARD,
    UNDEFINED
};

#endif
