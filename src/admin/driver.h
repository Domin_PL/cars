#ifndef PROJEKT_DRIVER_H
#define PROJEKT_DRIVER_H


#include <cstdio>
#include <exception>
#include <string>
#include "lights.h"
#include "door.h"
#include "../infrastructure/engine.h"
#include "position.h"
#include "../domain/util/StringFactory.h"

class Driver {

    class cannotTurnException : public std::exception {
public:
    cannotTurnException(char* message) {
        using namespace std;

        string baseMessage = "Cannot turn ";
        baseMessage.append(message)
                .append("! You can turn ")
                .append(Constants().LEFT)
                .append(" or ")
                .append(Constants().RIGHT)
                .append("\n");
        this->baseMessage = (char*) baseMessage.c_str();
    }

    const char* what() const throw() {
        return this->baseMessage;
    }

private:
    char* baseMessage;

};


public:
    Driver(Lights lights, Door door, Engine engine);

    class CannotDriveException : public std::exception {
    public:
        CannotDriveException(char* message) {
            using namespace std;

            this->baseMessage = message;
        }

        const char* what() const throw() {
            return this->baseMessage;
        }

    private:
        char* baseMessage;
    };

private:
    void printMessage(char*);
    Lights lights;
    Door door;
    Engine engine;

public:
    virtual void drive(Position);
    virtual void turn(Position);
    virtual void stop();
    virtual void turnOnTheCar();
    virtual bool isReadyToDrive();
    virtual void tankTheFuel(int);
    virtual void getIntoTheCar();
    virtual void openDoor(DoorType::position);
    virtual void closeDoor(DoorType::position);
    virtual void startEngine();
    virtual void stopEngine();
    virtual void turnOnDailyLights();
    virtual void turnOffDailyLights();
};

Driver::Driver(Lights lights, Door door, Engine engine) : engine(engine) {
    this->lights = lights;
    this->door = door;
    this->engine = engine;
}

void Driver::printMessage(char* message) {
    printf("Driving %s\n", message);
}

void Driver::drive(Position position) {
    if (!isReadyToDrive()) {
        throw CannotDriveException("Car is not ready to drive!");
    }
    if (position == FORWARD || position == BACKWARDS) {
        printMessage((char*) StringFactory().convertDrivingPositionToString(position));
    } else {
        using namespace std;
        string baseMessage = "Cannot drive ";
        baseMessage.append(StringFactory().convertDrivingPositionToString(position));
        throw CannotDriveException((char*) baseMessage.c_str());
    }
}

void Driver::turn(Position position) {
    if (!isReadyToDrive()) {
            throw cannotTurnException("Car is not ready to drive! Cannot turn!");
        }
    if (position == LEFT) {
        lights.useLeftWinker();
        this->printMessage((char *) StringFactory().convertDrivingPositionToString(position));
        return;
    } else if (position == RIGHT) {
        lights.useRightWinker();
        this->printMessage((char *) StringFactory().convertDrivingPositionToString(position));
        return;
    } else {
        throw cannotTurnException((char *) StringFactory().convertDrivingPositionToString(position));
    }
}

void Driver::turnOnTheCar() {
    if (!engine.isReady()) {
        engine.start();
    }
    if (!lights.areDailyLightsTurnedOn()) {
        lights.turnOnDailyLights();
    }
}

void Driver::stop() {
    lights.useBrakeLights();
    printf("Stopped the car");
}

bool Driver::isReadyToDrive() {
    if (!engine.isReady()) {
        throw CannotDriveException("Cannot drive! engine is not ready yet! Check if it's running");
    } else if (!lights.areDailyLightsTurnedOn()) {
        throw CannotDriveException("Please turn on your daily lights before starting a drive");
    } else if (!door.isAllDoorClosed()){
        throw CannotDriveException("Close the door before driving!");
    }
    return true;
}

void Driver::tankTheFuel(int litres) {
    engine.tank(litres);
}

void Driver::getIntoTheCar() {
    if (door.isAllDoorClosed()) {
        throw Door::ClosedDoorException("Cannot get in! No doors are opened.");
    }
    printf("getting into the car\n");
}

void Driver::openDoor(DoorType::position position) {
    door.openDoor(position);
}

void Driver::closeDoor(DoorType::position position) {
    door.closeDoor(position);
}

void Driver::startEngine() {
    engine.start();
};

void Driver::stopEngine() {
    engine.stop();
};

void Driver::turnOnDailyLights() {
    lights.turnOnDailyLights();
}

void Driver::turnOffDailyLights() {
    lights.turnOffDailyLights();
}

#endif
