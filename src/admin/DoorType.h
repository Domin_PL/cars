#ifndef PROJEKT_DOORTYPE_H
#define PROJEKT_DOORTYPE_H

namespace DoorType {
    enum position{
        LEFT,
        RIGHT,
        BOOT,
        UNDEFINED
    };
};

#endif
