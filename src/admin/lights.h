#ifndef PROJEKT_LIGHTS_H
#define PROJEKT_LIGHTS_H

#include <cstdio>

class Lights {

private:
    bool frontLights, backLights;

public:
    void turnOnDailyLights();
    void turnOffDailyLights();
    bool areDailyLightsTurnedOn();
    void useBrakeLights();
    void useLeftWinker();
    void useRightWinker();
};

void Lights::turnOnDailyLights() {
    frontLights = true;
    backLights = true;
    printf("daily lights are now turned on!\n");
}

void Lights::turnOffDailyLights() {
    frontLights = false;
    backLights = false;
    printf("daily lights are now turned off!\n");
}

bool Lights::areDailyLightsTurnedOn() {
    bool areTurnedOn = (frontLights && backLights);
    return areTurnedOn;
}

void Lights::useBrakeLights() {
    printf("Using brake lights...\n");
}

void Lights::useLeftWinker() {
    printf("Using left winker...\n");
}

void Lights::useRightWinker() {
    printf("Using right winker...\n");
}


#endif
