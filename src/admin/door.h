#ifndef PROJEKT_DOOR_H
#define PROJEKT_DOOR_H

#include <cstdio>
#include "DoorType.h"
#include "../domain/util/StringFactory.h"

class Door {

class OpenDoorException : public std::exception {

public:
    OpenDoorException(DoorType::position doorType) {
        using namespace std;

        string baseMessage = StringFactory().convertDoorTypeToString(doorType);
        baseMessage.append(" door is already opened!");
        this->baseMessage = (char*) baseMessage.c_str();
     }

    const char* what() const throw() {
        return this->baseMessage;
    }

private:
    char* baseMessage;
};

public:
    class ClosedDoorException : public std::exception {

public:
    ClosedDoorException(DoorType::position doorType) {
        using namespace std;

        string baseMessage = StringFactory().convertDoorTypeToString(doorType);
        baseMessage.append(" door is already closed!");
        this->baseMessage = (char*) baseMessage.c_str();
    }

public:
    ClosedDoorException(string message) {
        using namespace std;
        this->baseMessage = (char*) message.c_str();
    }

    const char* what() const throw() {
        return this->baseMessage;
    }

private:
    char* baseMessage;
};

private:
    bool leftDoor, rightDoor,boot ;

public:
    Door();
    bool isAllDoorClosed();
    void openDoor(DoorType::position);
    void closeDoor(DoorType::position);

};

Door::Door(){
    leftDoor = false;
    rightDoor = false;
    boot = false;
}

bool Door::isAllDoorClosed() {
    bool allClosed = (!leftDoor && !rightDoor && !boot);
    return allClosed;
}

void Door::openDoor(DoorType::position doorType) {
    char *whichDoor;
    switch (doorType) {
        case DoorType::position::LEFT: {
            if (leftDoor) {
                throw OpenDoorException(doorType);
            } else {
                leftDoor = true;
                whichDoor = (char*) Constants().LEFT;
                break;
            }
        }
        case DoorType::position::RIGHT: {
            if (rightDoor) {
                throw OpenDoorException(doorType);
            } else {
                whichDoor = (char*) Constants().RIGHT;
                rightDoor = true;
                break;
            }
        } case DoorType::position::BOOT: {
            if (boot) {
                throw OpenDoorException(doorType);
            } else {
                whichDoor = (char *) Constants().BOOT;
                boot = true;
                break;
            }
        }
        default:
            printf("Couldn't recognize doors!\n");
            return;
    }
    printf("Opened %s door\n", whichDoor);
}

void Door::closeDoor(DoorType::position doorType) {
    char* whichDoor;
    switch (doorType) {
        case DoorType::position::LEFT: {
            if (!leftDoor) {
                throw ClosedDoorException(doorType);
            }
            leftDoor = false;
            whichDoor = (char*) Constants().LEFT;
            break;
        }
        case DoorType::position::RIGHT: {
            if (!rightDoor) {
                throw ClosedDoorException(doorType);
            }
            rightDoor = false;
            whichDoor = (char*) Constants().RIGHT;
            break;
        } case DoorType::position::BOOT: {
            if (!boot) {
                throw ClosedDoorException(doorType);
            }
            boot = false;
            whichDoor = (char*) Constants().BOOT;
            break;
        }
    }
    printf("Closed %s door\n", whichDoor);
}

#endif
