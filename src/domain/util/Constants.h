#ifndef PROJEKT_CONSTANTS_H
#define PROJEKT_CONSTANTS_H

#include <string>

class Constants {

public:
    const char* LEFT = "left";
    const char* RIGHT = "right";
    const char* BOTTOM = "bottom";
    const char* FORWARD = "forward";
    const char* BOOT = "boot";
    const char* UNDEFINED = "undefined";
    const char* BACKWARDS = "backwards";
};

#endif
