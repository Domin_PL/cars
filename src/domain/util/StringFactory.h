#ifndef PROJEKT_STRINGFACTORY_H
#define PROJEKT_STRINGFACTORY_H

#include "../../admin/door.h"
#include "../../admin/driver.h"
#include "Constants.h"
#include "../../admin/DoorType.h"
#include "../../admin/position.h"

using namespace std;

class StringFactory {

public:
    static const char * convertDoorTypeToString(DoorType::position doorType) {
        switch (doorType){
            case DoorType::LEFT:
                return Constants().LEFT;
            case DoorType::RIGHT:
                return Constants().RIGHT;
            case DoorType::BOOT:
                return Constants().BOOT;
            default:
                return Constants().UNDEFINED;
        }
    }

    static const char * convertDrivingPositionToString(Position position) {
        switch (position) {
            case Position::LEFT:
                return Constants().LEFT;
            case Position::RIGHT:
                return Constants().RIGHT;
            case Position::FORWARD:
                return Constants().FORWARD;
            case Position::BACKWARDS:
                return Constants().BACKWARDS;
        }
    }

    static DoorType::position convertStringToPosition(string position){
        if (position == "left") {
            return DoorType::LEFT;
        } else if (position == "right") {
            return DoorType::RIGHT;
        } else if (position == "boot") {
            return DoorType::BOOT;
        } else {
            return DoorType::UNDEFINED;
        }
    }

    static Position convertStringToDrivingPosition(string position){
        if (position == Constants().LEFT) {
            return LEFT;
        } else if (position == Constants().RIGHT) {
            return RIGHT;
        } else if (position == Constants().FORWARD) {
            return FORWARD;
        } else if (position == Constants().BACKWARDS){
            return BACKWARDS;
        } else {
            return UNDEFINED;
        }
    }
};

#endif
