#ifndef PROJEKT_CAR_H
#define PROJEKT_CAR_H

#include <iostream>
#include "admin/driver.h"

using namespace std;

class Car : public Driver {

public:
    Car(Lights lights, Door door, Engine engine);
    const string &getModel() const;
    const string &getColor() const;
    const string &getMarka() const;
    void setMarka(std::string);

private:
    string marka, model, color;
};

Car::Car(Lights lights, Door door, Engine engine) : Driver(lights, door, engine) {
    printf("Please enter a proper car model: ");
    cin >> model;
    printf("Please enter car's colouring: ");
    cin >> color;
    printf("What's the capacity for the fuel? ");
    int maxCapacity;
    cin >> maxCapacity;
    engine.setMaxCapacity(maxCapacity);
}

const string &Car::getModel() const {
    return model;
}

const string &Car::getColor() const {
    return color;
}

const string &Car::getMarka() const {
    return this->marka;
}

void Car::setMarka(string marka) {
    this->marka = marka;
}

#endif
