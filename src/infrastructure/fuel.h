#ifndef PROJEKT_FUEL_H
#define PROJEKT_FUEL_H

class Fuel {

private:
    int maxCapacity = 30;
    int fuelLevel = 0;
    int getUnallocatedLitres();

public:
    void tank(int);
    int getCurrentFuelLevel();
    void setMaxCapacity(int maxCapacity);
    bool isFull();
    bool isEmpty();
};

int Fuel::getUnallocatedLitres() {
    return maxCapacity - fuelLevel;
}

void Fuel::tank(int litres) {
    unsigned int litresTanked = 0;
    if (litres <= getUnallocatedLitres()) {
        litresTanked = fuelLevel + litres;
        fuelLevel = fuelLevel + litres;
    } else {
        fuelLevel = maxCapacity;
        litresTanked = maxCapacity;
    }
    printf("Tanked %d litres successfully\n", litresTanked);
}

int Fuel::getCurrentFuelLevel() {
    return Fuel::fuelLevel;
}

void Fuel::setMaxCapacity(int maxCapacity) {
    this->maxCapacity = maxCapacity;
}

bool Fuel::isFull() {
    bool isFull = this->maxCapacity == getCurrentFuelLevel();
    return isFull;
}

bool Fuel::isEmpty() {
    bool isEmpty = getCurrentFuelLevel() == 0;
    return isEmpty;
}

#endif
