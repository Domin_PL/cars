#ifndef PROJEKT_FIAT_H
#define PROJEKT_FIAT_H

#include "../../car.h"

using namespace std;

class Fiat : public Car {

public:
    Fiat(Lights lights, Door door, Engine engine);
};

Fiat::Fiat(Lights lights, Door door, Engine engine) : Car(lights, door, engine) {
    setMarka("Fiat");
}

#endif
