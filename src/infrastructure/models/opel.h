#ifndef PROJEKT_OPEL_H
#define PROJEKT_OPEL_H

#include "../../car.h"

using namespace std;

class Opel : public Car {

public:
    Opel(Lights lights, Door door, Engine engine);
};

Opel::Opel(Lights lights, Door door, Engine engine) : Car(lights, door, engine) {
    setMarka("Opel");
}

#endif
