#ifndef PROJEKT_MAZDA_H
#define PROJEKT_MAZDA_H

#include "../../car.h"

using namespace std;

class Mazda : public Car {

public:
    Mazda(Lights lights, Door door, Engine engine);
};

Mazda::Mazda(Lights lights, Door door, Engine engine) : Car(lights, door, engine) {
    setMarka("Mazda");
}

#endif
