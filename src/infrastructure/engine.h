#include "fuel.h"

#ifndef PROJEKT_ENGINE_H
#define PROJEKT_ENGINE_H

#include <cstdio>
#include <csignal>

class Engine : public Fuel {

    class CannotStartEngineException : public std::exception {

    public:
        CannotStartEngineException(char* message) {
            using namespace std;

            string baseMessage = message;
            baseMessage.append("\n");
            this->baseMessage = (char*) baseMessage.c_str();
        }

        const char* what() const throw() {
            return this->baseMessage;
        }

    private:
        char* baseMessage;
    };

private:
    bool isRunning;
    bool isBroken;

public:
    Engine();
    bool isReady();
    void start();
    void stop();
};

Engine::Engine() {
    isRunning = false;
    isBroken = false;
}
bool Engine::isReady() {
    return isRunning;
}

void Engine::start() {
    if (isRunning) {
        throw CannotStartEngineException("Engine is already running!");
    } else if (isBroken) {
        throw CannotStartEngineException("Engine is broken - cannot start");
    } else if (isEmpty()) {
        throw CannotStartEngineException("Cannot start engine - there is no fuel");
    } else {
        isRunning = true;
        printf("Engine has been started\n");
    }
}

void Engine::stop() {
    if (!isRunning) {
        printf("Cannot stop engine - it is not running!\n");
    } else {
        isRunning = false;
        printf("Engine has been stopped\n");
    }
}

#endif
