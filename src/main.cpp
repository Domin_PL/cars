#include <iostream>
#include <list>

#include "admin/driver.h"
#include "car.h"
#include "infrastructure/models/mazda.h"
#include "infrastructure/models/fiat.h"
#include "infrastructure/models/opel.h"

using namespace std;

list<Car*> cars;
Car* activeCar;
Lights lights = Lights();
Door door = Door();
Engine engine = Engine();

class UndefinedActionException : public exception {

public:
    UndefinedActionException(string action) {
        using namespace std;

        string baseMessage = "Undefined action: ";
        baseMessage.append(action)
                   .append("\n");
        this->baseMessage = (char*) baseMessage.c_str();
    }

    const char* what() const throw() {
        return this->baseMessage;
    }

private:
    char* baseMessage;

};

void printHelp(bool carActions = false) {
    string message;
    if (carActions) {
        message =
                "Here's the list for actions taken to an active car:\n"
                "[open|close] [left|right|boot] - opens/closes specified doors\n"
                "get in - gets driver into a car\n"
                "tank - tanks fuel to the car\n"
                "[start|stop] engine - starts/stops engine\n"
                "drive [forward|backwards] - drives forwards or backwards\n"
                "turn [left|right] - turns specified direction. Note that the car must be turned on.\n"
                "turn [on|off] lights - turns on/off daily lights.\n";

    } else {
        message =
                "h - prints help\n"
                "n - opens a new car wizard\n"
                "l - lists current cars\n"
                "c - chooses a car from the list\n"
                "m - lists actions for a car\n"
                "d - deletes a car";
    }

    cout << message << endl;
}

void createCar(){
    Car* car;
    printf("What brand car is going to be? (Mazda, Fiat, Opel): ");
    string brand;
    cin >> brand;
    if (brand == "Mazda") {
        car = new Mazda(lights, door, engine);
    } else if (brand == "Fiat") {
        car = new Fiat(lights, door, engine);
    } else if (brand == "Opel") {
        car = new Opel(lights, door, engine);
    } else {
        printf("Unknown brand. Available choices are: Mazda, Fiat, Opel\n");
        return;
    }
    cars.push_back(car);
    printf("Car has been added succesfully\n");
}


void listCars(){
    if (cars.empty()) {
        printf("No cars in the list\n");
        return;
    }
    list<Car*> :: iterator it;
    int index = 1;
    for (it = cars.begin(); it != cars.end(); ++it) {
        auto* car = it.operator*();
        if (activeCar == car) {
            printf("%d. %s %s %s [ACTIVE]\n", index, car->getColor().c_str(), car->getMarka().c_str(), car->getModel().c_str());
        } else {
            printf("%d. %s %s %s\n", index, car->getColor().c_str(), car->getMarka().c_str(), car->getModel().c_str());
        }
        index++;
    }

}

void changeActiveCar() {
    int decision = 0;
    if (cars.empty()) {
        printf("There are no cars added yet\n");
        return;
    }
    printf("Choose an active car from the list: \n");
    listCars();
    printf("your decision: ");
    cin >> decision;
    list<Car*> :: iterator it;
    int index = 1;
    for (it = cars.begin(); it != cars.end(); ++it) {
        auto* car = it.operator*();
        if (index == decision) {
            printf("%d. %s %s %s has been set to active car\n", index, car->getColor().c_str(), car->getMarka().c_str(), car->getModel().c_str());
            activeCar = car;
            return;
        }
        index++;
    }
}

void deleteCar() {
    if (cars.empty()) {
        printf("There are no cars added yet - nothing to delete\n");
        return;
    }
    int decision = 0;
    printf("Choose a car to delete: \n");
    listCars();
    printf("your decision: ");
    cin >> decision;
    list<Car*> :: iterator it;
    int index = 1;
    for (it = cars.begin(); it != cars.end(); ++it) {
        auto* car = it.operator*();
        if (index == decision) {
            string carToDelete = car->getColor();
            carToDelete.append(" ")
                        .append(car->getMarka())
                        .append(" ")
                        .append(car->getModel());

            if (car == activeCar) {
                printf("This is an active car. Before deleting an active car, choose another one\n");
                changeActiveCar();
            }
            cars.remove(car);
            delete car;
            printf("%s has been deleted successfully\n", carToDelete.c_str());
            return;
        }
        index++;
    }
}

void handleDoor(bool closeDoor, DoorType::position whichDoor) {
    if (closeDoor) {
        activeCar->closeDoor(whichDoor);
    } else {
        activeCar->openDoor(whichDoor);
    }
}

void handleEngine(bool startEngine) {
    if (startEngine) {
        activeCar->startEngine();
    } else {
        activeCar->stopEngine();
    }
}

string* splitInputIntoStringArray(string input) {
    string word = "";
    string* splittedInput = new string[3];
    int index = 0;
    for (int i = 0; i < input.length(); i++) {
        char x = input.at(i);

        if (index == 3) {
            printf("Input is too long! Validating only: %s %s %s\n", splittedInput[0].c_str(), splittedInput[1].c_str(), splittedInput[2].c_str());
            return splittedInput;
        }
        if (x != ' ') {
            word = word + x;
        }
        if (x == ' ' || i + 1 == input.length()) {
            splittedInput[index] = word;
            index++;
            word = "";
        }

    }
    return splittedInput;

}

void handleInput(string input) {
    if (input.length() == 0) {
        return;
    }
    if (input.length() == 1) {
        char c = input.at(0);
        switch (c) {
            case 'h':
                printHelp();
                break;
            case 'n':
                createCar();
                break;
            case 'l':
                listCars();
                break;
            case 'c':
                changeActiveCar();
                break;
            case 'm':
                printHelp(true);
                break;
            case 'd':
                deleteCar();
                break;
            default:
                printf("Undefined action. Here's the list of the available commands:\n");
                printHelp();
        }
    } else {
        if (activeCar == nullptr) {
            if (cars.size() == 0) {
                printf("There is no car added yet - create one before taking any action\n");
            } else {
                printf("There is no active car - choose one before doing any action on it!\n");
                changeActiveCar();
            }
            return;
        }
        string* splittedInput = splitInputIntoStringArray(input);
        string firstInput = splittedInput[0];
        string secondInput = splittedInput[1];
        string thirdInput = splittedInput[2];
        if (firstInput == "open" || firstInput == "close") {
            bool closeDoor = firstInput == "close";
            handleDoor(closeDoor, StringFactory().convertStringToPosition(secondInput));
        } else if (firstInput == "get") {
            if (secondInput != "in") {
                string error = firstInput + " " + secondInput;
                throw UndefinedActionException(error);
            }
            activeCar->getIntoTheCar();
        } else if (firstInput == "tank") {
            unsigned int litres;
            printf("How many litres of fuel would you like to tank?: ");
            cin >> litres;
            activeCar->tankTheFuel(litres);
        } else if (firstInput == "start" || firstInput == "stop") {
            if (secondInput != "engine") {
                string error = firstInput + " " + secondInput;
                throw UndefinedActionException(error);
            }
            handleEngine(firstInput == "start");
        } else if (firstInput == "drive") {
            activeCar->drive(StringFactory().convertStringToDrivingPosition(secondInput));
        } else if (firstInput == "turn") {
            if (secondInput == "on" || secondInput == "off") {
                if (thirdInput != "lights") {
                    string error = firstInput + " " + secondInput + " " + thirdInput;
                    throw UndefinedActionException(error);
                }
                if (secondInput == "on") {
                    activeCar->turnOnDailyLights();
                } else {
                    activeCar->turnOffDailyLights();
                }
            } else {
                activeCar->turn(StringFactory().convertStringToDrivingPosition(secondInput));
            }
        } else {
            throw UndefinedActionException(firstInput);
        }
    }

}



int main() {
    printf("Hello :) \nHere's the list of available commands:\n");
    printHelp();
    string input;
    while (true) {
        getline(cin, input);
        try {
            handleInput(input);
        } catch (std::exception& e) {
            printf("an exception has occurred: %s", e.what());
        }
    }

    return 0;
}
